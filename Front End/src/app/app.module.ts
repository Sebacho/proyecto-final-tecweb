import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule,  HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { IndexComponent } from './components/index/index.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CharacterItemComponent } from './components/character-item/character-item.component';
import { AddCharacterComponent } from './components/add-character/add-character.component';
import { SkillsComponent } from './components/skills/skills.component';
import { SkillItemComponent } from './components/skill-item/skill-item.component';
import { AddSkillComponent } from './components/add-skill/add-skill.component';
import { HomeComponent } from './components/pages/home/home.component';
import { CreateCharacterComponent } from './components/pages/create-character/create-character.component';
import { EditCharacterComponent } from './components/edit-character/edit-character.component';
import { EditSkillComponent } from './components/edit-skill/edit-skill.component';
import { SignUpComponent } from './components/pages/sign-up/sign-up.component';
import { LogInComponent } from './components/pages/log-in/log-in.component';
import { CreateSkillComponent } from './components/pages/create-skill/create-skill.component';
import { RacesComponent } from './components/pages/races/races.component';
import { ClassesComponent } from './components/pages/classes/classes.component';
import { MigrateModalComponent } from './components/migrate-modal/migrate-modal.component';
import { EditCharacterModalComponent } from './components/edit-character-modal/edit-character-modal.component';
import { ParallaxDirective } from './parallax.directive';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HallFameComponent } from './components/hall-fame/hall-fame.component';
import { EditSkillModalComponent } from './components/edit-skill-modal/edit-skill-modal.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    IndexComponent,
    CharactersComponent,
    CharacterItemComponent,
    AddCharacterComponent,
    SkillsComponent,
    SkillItemComponent,
    AddSkillComponent,
    HomeComponent,
    CreateCharacterComponent,
    EditCharacterComponent,
    EditSkillComponent,
    SignUpComponent,
    LogInComponent,
    CreateSkillComponent,
    RacesComponent,
    ClassesComponent,
    MigrateModalComponent,
    EditCharacterModalComponent,
    ParallaxDirective,
    HallFameComponent,
    EditSkillModalComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents:[MigrateModalComponent,EditCharacterModalComponent]
})
export class AppModule { }
