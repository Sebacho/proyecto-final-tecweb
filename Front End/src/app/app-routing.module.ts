import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharactersComponent } from './components/characters/characters.component';
import { HomeComponent } from './components/pages/home/home.component';
import { CreateCharacterComponent } from './components/pages/create-character/create-character.component';
import { SkillsComponent } from './components/skills/skills.component';
import { EditCharacterComponent } from './components/edit-character/edit-character.component';
import { LogInComponent } from './components/pages/log-in/log-in.component';
import { CreateSkillComponent } from './components/pages/create-skill/create-skill.component';
import { EditSkillComponent } from './components/edit-skill/edit-skill.component';
import { HallFameComponent } from './components/hall-fame/hall-fame.component';


const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'character',component:CharactersComponent},
  {path:'newCharacter',component:CreateCharacterComponent},
  {path:'newSkill/:characterId',component:CreateSkillComponent},
  {path:'characters/:characterId',component:EditCharacterComponent},
  {path:'characters/:characterId/skills',component:SkillsComponent},
  {path:'login',component:LogInComponent},
  {path:'HallOfFame',component:HallFameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
