import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';
import { Router } from '@angular/router';
import { EditCharacterModalComponent } from '../edit-character-modal/edit-character-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-character-item',
  templateUrl: './character-item.component.html',
  styleUrls: ['./character-item.component.css']
})
export class CharacterItemComponent implements OnInit {

  @Input() characterInput:Character;
  @Output() characterDeleteOutput:EventEmitter<any>= new EventEmitter();
  constructor(private characterService:CharacterService, private router:Router,private modalService: NgbModal) { }

  ngOnInit(): void {
  }
  onDelete(){ 
    this.characterDeleteOutput.emit(this.characterInput);
  }
  onEdit(){
    //this.router.navigateByUrl(`/characters/${this.characterInput.id}`);
    const modalRef = this.modalService.open(EditCharacterModalComponent);
    modalRef.componentInstance.characterId = this.characterInput.id;
  }
  onSkill(){
    this.router.navigateByUrl(`/characters/${this.characterInput.id}/skills`);
  }
  onImageHover(){
    console.log("hola mundi");
  }
  onFame(){
    this.characterInput.fame+=1;
    this.characterService.updateCharacter(this.characterInput).subscribe(t=>{
    console.log(t);
  }); 
   }
}
