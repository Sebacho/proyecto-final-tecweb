import { Component, OnInit } from '@angular/core';
import { CharacterService } from 'src/app/services/character.service';
import { Character } from 'src/app/models/Character';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-character',
  templateUrl: './edit-character.component.html',
  styleUrls: ['./edit-character.component.css']
})
export class EditCharacterComponent implements OnInit {

  character:Character;
  constructor(private characterService:CharacterService,private route:ActivatedRoute,private router:Router) { 
    this.character=new Character();
  }

  ngOnInit(): void {
    
     const characterId=this.route.snapshot.paramMap.get("characterId");
     console.log(characterId);
     this.characterService.getCharacter(characterId).subscribe(t=>{
      this.character=t;
      console.log(t);
    });
    console.log("entra1");
  }
  onSubmit(character:Character){
    console.log("entra2");
    // const characterId=this.route.snapshot.paramMap.get("characterId");
    this.characterService.updateCharacter(character).subscribe(t=>{
      console.log(t);
    });
  }
  onBack(){
    
    this.router.navigateByUrl(`/character`);
   }
}
