import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.css']
})
export class AddSkillComponent implements OnInit {

  name:string;
  description:string;
  imgSkillUrl:string;
  @Output() addSkill:EventEmitter<any> = new EventEmitter();
  constructor(private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    const skill= {
      name:this.name,
      description:this.description,
      imgSkillUrl:this.imgSkillUrl
    }
    this.addSkill.emit(skill);
 }
  onBack(){
   const charId=this.route.snapshot.paramMap.get("characterId");
   this.router.navigateByUrl(`/characters/${charId}/skills`);
  }
}
