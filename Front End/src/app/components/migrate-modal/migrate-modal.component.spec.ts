import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MigrateModalComponent } from './migrate-modal.component';

describe('MigrateModalComponent', () => {
  let component: MigrateModalComponent;
  let fixture: ComponentFixture<MigrateModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MigrateModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MigrateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
