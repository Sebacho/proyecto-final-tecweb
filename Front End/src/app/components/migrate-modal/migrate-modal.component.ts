import { Component, OnInit,Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SkillService } from 'src/app/services/skill.service';

@Component({
  selector: 'app-migrate-modal',
  templateUrl: './migrate-modal.component.html',
  styleUrls: ['./migrate-modal.component.css']
})
export class MigrateModalComponent implements OnInit {
  @Input() public characterId;
  @Input() public skillId;
  characterName:string;
  constructor(public activeModal: NgbActiveModal,private skillService:SkillService) { }

  ngOnInit(): void {
  }
  onMigrate(){
    console.log("name:"+this.characterName);
    console.log("characterId:"+this.characterId);
    console.log("skillId"+this.skillId);
    this.skillService.migrateSkill(this.characterId,this.skillId,this.characterName).subscribe();
    this.activeModal.close();
    window.location.reload();
    }

}
