import { Component, OnInit,Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SkillService } from 'src/app/services/skill.service';
import { Skill } from 'src/app/models/Skill';

@Component({
  selector: 'app-edit-skill-modal',
  templateUrl: './edit-skill-modal.component.html',
  styleUrls: ['./edit-skill-modal.component.css']
})
export class EditSkillModalComponent implements OnInit {

  @Input() public skillId;
  @Input() public characterId;
  skill:Skill;
  constructor(public activeModal:NgbActiveModal, private skillService:SkillService) {
    this.skill= new Skill();
   }

  ngOnInit(): void {
    this.skillService.getSkill(this.characterId,this.skillId).subscribe(t=>{
      this.skill=t;
      console.log(t);
    });
  }
  onSubmit(skill:Skill){
    console.log("entra2");
    // const characterId=this.route.snapshot.paramMap.get("characterId");
    this.skillService.updateSkill(skill.characterId,skill).subscribe(t=>{
      console.log(t);
    });
    this.activeModal.close();
    window.location.reload();
  }

}
