import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSkillModalComponent } from './edit-skill-modal.component';

describe('EditSkillModalComponent', () => {
  let component: EditSkillModalComponent;
  let fixture: ComponentFixture<EditSkillModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSkillModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSkillModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
