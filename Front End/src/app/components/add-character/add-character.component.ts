import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-character',
  templateUrl: './add-character.component.html',
  styleUrls: ['./add-character.component.css']
})
export class AddCharacterComponent implements OnInit {

  name:string;
  itemLvl:number;
  class:string;
  server:string;
  fame:number;
  imgUrl:string;
  @Output() addCharacter:EventEmitter<any> = new EventEmitter();
  constructor(private router:Router,private route:ActivatedRoute) {
    this.fame=0;
  }

  ngOnInit(): void {
  }
  onSubmit(){
    const character= {
      name:this.name,
      itemLvl:this.itemLvl,
      class:this.class,
      server:this.server,
      fame:this.fame,
      imgUrl:this.imgUrl 
    }
    this.addCharacter.emit(character);
 }
  onBack(){
    this.router.navigateByUrl(`/character`);
  }
}
