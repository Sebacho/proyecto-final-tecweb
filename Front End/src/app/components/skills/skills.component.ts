import { Component, OnInit } from '@angular/core';
import { Skill } from 'src/app/models/Skill';
import { SkillService } from 'src/app/services/skill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CharacterService } from 'src/app/services/character.service';
import { Character } from 'src/app/models/Character';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  skills:Skill[];
  character:Character;
  constructor(private characterService:CharacterService ,private skillService:SkillService, private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    const characterId=this.route.snapshot.paramMap.get("characterId");
    this.characterService.getCharacter(characterId).subscribe(character=>{this.character=character});
    this.skillService.getSkills(characterId).subscribe(skill=>{
      this.skills=skill;
    });
  }
  deleteSkill(skillDelete:Skill):void{
    this.skills=this.skills.filter(t=>t.id !==skillDelete.id)
    //delete from be
    const characterId=this.route.snapshot.paramMap.get("characterId");
    this.skillService.deleteSkill(characterId,skillDelete).subscribe();
  }
  createSkill(){
    console.log("entra");
     const charId=this.route.snapshot.paramMap.get("characterId");
     this.router.navigateByUrl(`/newSkill/${charId}`);
  }
  onBack(){
    
    this.router.navigateByUrl(`/character`);
   }
  
}
