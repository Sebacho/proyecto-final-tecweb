import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Skill } from 'src/app/models/Skill';
import { SkillService } from 'src/app/services/skill.service';
import { Router } from '@angular/router';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MigrateModalComponent } from '../migrate-modal/migrate-modal.component';
import { EditSkillModalComponent } from '../edit-skill-modal/edit-skill-modal.component';


@Component({
  selector: 'app-skill-item',
  templateUrl: './skill-item.component.html',
  styleUrls: ['./skill-item.component.css']
})
export class SkillItemComponent implements OnInit {

  characters:Character[];
  @Input() skillInput:Skill;
  @Output() skillDeleteOutput:EventEmitter<any>= new EventEmitter();
  constructor(private skillService:SkillService,private router:Router, private characterService:CharacterService,private modalService: NgbModal ) { }

  ngOnInit(): void {
    if(this.characterService.getCharacters("?OrderBy=id"," "," ",3).subscribe(character=>{
      this.characters=character;
      console.log("character"+this.characters);
    })==null)
    {
      alert("No se encontro");
    }
  }
  onDelete(){ 
    this.skillDeleteOutput.emit(this.skillInput);
  }
  onEdit(){
    const modalRef = this.modalService.open(EditSkillModalComponent);
    modalRef.componentInstance.skillId = this.skillInput.id;
    modalRef.componentInstance.characterId=this.skillInput.characterId;
    // this.router.navigateByUrl(`/characters/${this.skillInput.characterId}/skills/${this.skillInput.id}`);
  }
  onMigrate(){
    const modalRef = this.modalService.open(MigrateModalComponent);
    modalRef.componentInstance.characterId = this.skillInput.characterId;
    modalRef.componentInstance.skillId = this.skillInput.id;
    this.ngOnInit();
    // console.log("nombre"+this.characterName);
    // this.skillService.migrateSkill(this.skillInput.characterId,this.skillInput.id,this.characterName);
  }
}
