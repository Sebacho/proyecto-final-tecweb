import { Component, OnInit } from '@angular/core';
import { Skill } from 'src/app/models/Skill';
import { ActivatedRoute, Router } from '@angular/router';
import { SkillService } from 'src/app/services/skill.service';

@Component({
  selector: 'app-edit-skill',
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.css']
})
export class EditSkillComponent implements OnInit {

  skill:Skill;
  constructor(private skillService:SkillService,private route:ActivatedRoute,private router:Router) { 
    this.skill=new Skill();
  }

  ngOnInit(): void {
    const skillId:string=this.route.snapshot.paramMap.get("skillId");
    const characterId:string=this.route.snapshot.paramMap.get("characterId");
    console.log("skillID: "+skillId);
    console.log("characterID: "+characterId);
    this.skillService.getSkill(characterId,skillId).subscribe(t=>{
      this.skill=t;
      console.log("objeto: "+t.description);
    });
  }
  onSubmit(skill:Skill){
    // console.log("entra2");
    //const characterId=this.route.snapshot.paramMap.get("characterId");
     this.skillService.updateSkill(skill.characterId,skill).subscribe(t=>{
       console.log(t);
     });
  }
  onBack(){
    const charId=this.route.snapshot.paramMap.get("characterId");
    this.router.navigateByUrl(`/characters/${charId}/skills`);
   }
}
