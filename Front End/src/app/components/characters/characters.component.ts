import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';
import { Router } from '@angular/router';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  characters:Character[];
  characterService:CharacterService;
  orderBy:string;
  searchBy:string;
  searchFor:string;
  inputSearch:string;

  constructor(characterService:CharacterService,private router:Router) {
    this.characterService=characterService;
    this.orderBy="?OrderBy=id";
    this.searchBy=" ";
   }

  ngOnInit(): void {
    console.log("ngoninit:"+this.orderBy);
    if(this.characterService.getCharacters(this.orderBy,this.searchBy,this.searchFor,0).subscribe(character=>{
      this.characters=character;
    })==null)
    {
      alert("No se encontro");
    }
  }
  deleteCharacter(characterDelete:Character):void{
    this.characters=this.characters.filter(t=>t.id !==characterDelete.id)
    //delete from be
    this.characterService.deleteCharacter(characterDelete).subscribe();
  }
  onChange(deviceValue) {
    this.orderBy="?OrderBy="+deviceValue;
  }
  onChange2(deviceValue){
    this.searchBy="searchBy="+deviceValue;
  }
  onSearch(){
    if (this.inputSearch=="Without Filter"){
      this.inputSearch=" ";
    }
    console.log(this.inputSearch);
    this.searchFor="searchFor="+this.inputSearch;
    this.ngOnInit();
  }
 
}
