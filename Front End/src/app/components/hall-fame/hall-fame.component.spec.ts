import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HallFameComponent } from './hall-fame.component';

describe('HallFameComponent', () => {
  let component: HallFameComponent;
  let fixture: ComponentFixture<HallFameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HallFameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HallFameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
