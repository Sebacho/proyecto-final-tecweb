import { Component, OnInit,Input } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CharactersComponent } from '../characters/characters.component';

@Component({
  selector: 'app-edit-character-modal',
  templateUrl: './edit-character-modal.component.html',
  styleUrls: ['./edit-character-modal.component.css']
})
export class EditCharacterModalComponent implements OnInit {

  @Input() public characterId;
  character:Character;
  constructor(public activeModal:NgbActiveModal, private characterService:CharacterService,private route:ActivatedRoute,private router:Router) {
    this.character=new Character();
   }

  ngOnInit(): void {
    this.characterService.getCharacter(this.characterId).subscribe(t=>{
      this.character=t;
      console.log(t);
    });
  }
  onSubmit(character:Character){
    console.log("entra2");
    // const characterId=this.route.snapshot.paramMap.get("characterId");
    this.characterService.updateCharacter(character).subscribe(t=>{
      console.log(t);
    });
    this.activeModal.close();
    window.location.reload();
  }

}
