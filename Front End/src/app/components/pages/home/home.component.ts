import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  characters:Character[];
  orderBy:string;
  searchBy:string;
  searchFor:string;
  constructor(private characterService:CharacterService,private router:Router) {
    this.orderBy="?OrderBy=Id";
    this.searchBy=" ";
   }

  ngOnInit(): void {
    if(this.characterService.getCharacters(this.orderBy,this.searchBy,this.searchFor,5).subscribe(character=>{
      this.characters=character;
    })==null)
    {
      alert("No se encontro");
    }
  }
  onSkill()
  {

  }
  onFame()
  {
    this.router.navigateByUrl(`/HallOfFame`);
  }
}
