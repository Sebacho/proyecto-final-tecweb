import { Component, OnInit } from '@angular/core';
import { SkillService } from 'src/app/services/skill.service';
import { ActivatedRoute } from '@angular/router';
import { Skill } from 'src/app/models/Skill';

@Component({
  selector: 'app-create-skill',
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.css']
})
export class CreateSkillComponent implements OnInit {

  skills:Skill[];
  constructor(private skillService:SkillService,private route:ActivatedRoute) { }

  ngOnInit(): void {
  }
  addSkillAndSend(skillAdd:Skill){
    const characterId=this.route.snapshot.paramMap.get("characterId");
    console.log(characterId);
    this.skillService.addSkill(characterId,skillAdd).subscribe(skill=>{
      this.skills.push(skill);
    });
  }
}
