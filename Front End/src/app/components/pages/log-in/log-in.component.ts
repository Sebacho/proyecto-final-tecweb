import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {
  
  //  email:string;
  //  password:string;

  loginUserData = {
    // "email":this.email,
    // "password":this.password
  }
  constructor(private _auth: AuthService, private _router: Router) { }

  ngOnInit(): void {
  }

  loginUser () {
   this._auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
        localStorage.setItem('message', res.message)
        
      },
       err => console.log(err)
    ) 
      // if (log[1]==false)
      // {
      //   alert("Email o Contraseña incorrectos");
      // }
      // else{
      //   this._router.navigateByUrl(`/`);
      // }
  }
}
