import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/models/Character';
import { CharacterService } from 'src/app/services/character.service';

@Component({
  selector: 'app-create-character',
  templateUrl: './create-character.component.html',
  styleUrls: ['./create-character.component.css']
})
export class CreateCharacterComponent implements OnInit {

  characters:Character[];
  constructor(private characterService:CharacterService) { }

  ngOnInit(): void {
  }
  addCharacterAndSend(characterAdd:Character){
    this.characterService.addCharacter(characterAdd).subscribe(character=>{
      this.characters.push(character);
    });
  }

}
