import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Skill } from '../models/Skill';
import { Character } from '../models/Character';

const httpOptions ={
  headers: new HttpHeaders({
    'content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class SkillService {
  skillUrl:string="https://localhost:44308/api/characters";
  constructor(private http:HttpClient) { }
  getSkills(characterId:string):Observable<Skill[]>{
    return this.http.get<Skill[]>(this.skillUrl+`/${characterId}`+"/skills");
  }
  updateSkill(characterId:number,skill:Skill):Observable<any>{
    return this.http.put(this.skillUrl+`/${characterId}`+"/skills"+`/${skill.id}`,skill,httpOptions);
  }
  deleteSkill(characterId:string,skill:Skill):Observable<any>{
    return this.http.delete(this.skillUrl+`/${characterId}`+"/skills"+`/${skill.id}`);
  }
  addSkill(characterId:string, skill:Skill):Observable<any>{
    console.log(skill);
    return this.http.post<Skill>(this.skillUrl+`/${characterId}`+"/skills",skill,httpOptions);
  }
  getSkill(characterId:string,id:string):Observable<Skill>{
    return this.http.get<Skill>(`${this.skillUrl}/${characterId}`+"/skills"+`/${id}`);
  }
  migrateSkill(characterId:number,skillId:number,characterName:string):Observable<any>{
    console.log("SERVICE");
    console.log("charId:"+characterId);
    console.log("skillId:"+skillId);
    console.log("charName:"+characterName);
    return this.http.delete(`${this.skillUrl}/${characterId}`+"/skills"+`/${skillId}`+"/Migrate"+`/${characterName}`);
  }
}
