import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router'

@Injectable()
export class AuthService {

  private _loginUrl:string="https://localhost:44308/api/auth/login";
  constructor(private http: HttpClient,private _router: Router) { }

  loginUser(user) {
    return this.http.post<any>(this._loginUrl, user)
  }

  getToken() {
    return localStorage.getItem('message')
  }
}
