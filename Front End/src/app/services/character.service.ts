import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Character } from '../models/Character';

const httpOptions ={
  headers: new HttpHeaders({
    'content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  characterUrl:string="https://localhost:44308/api/characters";
  constructor(private http:HttpClient) { }
  getCharacters(orderBy:string,searchBy:string,searchFor:string,famous:number):Observable<Character[]>{
    console.log("fame:"+famous);
    return this.http.get<Character[]>(this.characterUrl+orderBy+"&"+searchBy+"&"+searchFor+"&famous="+famous);
  }
  updateCharacter(character:Character):Observable<any>{
    console.log(character);
    //console.log(charId);
    return this.http.put(this.characterUrl+`/${character.id}`,character,httpOptions);
  }
  deleteCharacter(character:Character):Observable<any>{
    return this.http.delete(this.characterUrl+`/${character.id}`);
  }
  addCharacter(character:Character):Observable<any>{
    return this.http.post<Character>(this.characterUrl,character,httpOptions);
  }
  getCharacter(id:string):Observable<Character>{
    return this.http.get<Character>(`${this.characterUrl}/${id}`);
  }
  votesCharacter(character:Character):Observable<any>{
    console.log("charService: "+character.name);
    return this.http.put(this.characterUrl+"/Fame"+`/${character.id}`,httpOptions);
    // https://localhost:44308/api/characters/Fame/2
  }
  
}
