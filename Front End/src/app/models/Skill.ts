export class Skill{
    id?:number;
    name:string;
    description:string;
    characterId:number;
    imgSkillUrl:string;
}