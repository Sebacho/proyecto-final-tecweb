
export class Character {
    id?:number;
    name:string;
    itemLvl?:number;
    class:string;
    server:string;
    fame:number;
    imgUrl:string;
}