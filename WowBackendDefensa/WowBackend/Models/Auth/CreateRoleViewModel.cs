﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WowBackend.Models.Auth
{
    public class CreateRoleViewModel
    {
        [Required]
        public string name { get; set; }
        [Required]
        public string NormalizedName { get; set; }
    }
}
