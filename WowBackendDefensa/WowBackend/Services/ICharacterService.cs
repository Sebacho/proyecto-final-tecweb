﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Models;

namespace WowBackend.Services
{
    public interface ICharacterService
    {
        Task<CharacterModel> GetCharacterAsync(int id, bool showSkills = false);
        Task<IEnumerable<CharacterModel>> GetCharactersAsync(string searchBy = "Id", string searchFor = " ", string orderBy = "Id", bool showSkills = false, int famoues = 0);
        Task<CharacterModel> CreateCharacterAsync(CharacterModel newCharacter);
        Task<bool> UpdateCharacterAsync(CharacterModel character, int id);
        Task<bool> DeleteCharacterAsync(int id);
        Task<IEnumerable<SkillModel>> GetSkillsId(int id);
        Task<bool> AddVoteCharacterAsync(int id);
    }
}
