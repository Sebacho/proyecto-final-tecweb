﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Models;

namespace WowBackend.Services
{
    public interface ISkillService
    {
        Task<SkillModel> GetSkillAsync(int characterId, int id);
        Task<IEnumerable<SkillModel>> GetSkillsAsync(int characterId);
        Task<SkillModel> CreateSkillAsync(int characterId, SkillModel newSkill);
        Task<bool> UpdateSkillAsync(int characterId, SkillModel skill, int id);
        Task<bool> DeleteSkillAsync(int characterId, int id);
    }
}
