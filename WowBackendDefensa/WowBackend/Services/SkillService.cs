﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;
using WowBackend.Data.Repository;
using WowBackend.Exceptions;
using WowBackend.Models;

namespace WowBackend.Services
{
    public class SkillService : ISkillService
    {
        private IWorldOfWarcraftRepository repository;
        private readonly IMapper mapper;
        public SkillService(IWorldOfWarcraftRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task<SkillModel> CreateSkillAsync(int characterId, SkillModel newSkill)
        {
            await validateCharacterAsync(characterId);
            newSkill.CharacterId = characterId;
            var skillEntity = mapper.Map<SkillEntity>(newSkill);
            //prueba
            //var characterEntity = repository.GetCharacterAsync(characterId);
            //characterEntity.  Skills.Add(skillEntity);

            repository.CreateSkill(skillEntity);
            var res = await repository.SaveChangesAsync();
            if (res)
            {
                return mapper.Map<SkillModel>(skillEntity);
            }

            throw new Exception("Database Exception");


        }

        public async Task<bool> DeleteSkillAsync(int characterId, int id)
        {
            await GetSkillAsync(characterId, id);
            await repository.DeleteSkillAsync(id);
            var res = await repository.SaveChangesAsync();
            if (res)
            {
                return true;
            }

            throw new Exception("Database Exception");
        }

        public async Task<SkillModel> GetSkillAsync(int characterId, int id)
        {
            await validateCharacterAsync(characterId);
            var skill = await repository.GetSkillAsync(id);
            if (skill == null || skill.Character.Id != characterId)
            {
                throw new NotFoundException($"the skill with the id: {id} doesn't exist");
            }
            return mapper.Map<SkillModel>(skill);
        }

        public async Task<IEnumerable<SkillModel>> GetSkillsAsync(int characterId)
        {
            await validateCharacterAsync(characterId);
            return mapper.Map<IEnumerable<SkillModel>>(await repository.GetSkillsAsync(characterId));
        }

        public async Task<bool> UpdateSkillAsync(int characterId, SkillModel skill, int id)
        {
            await GetSkillAsync(characterId, id);
            skill.Id = id;
            skill.CharacterId = characterId;
            repository.UpdateSkill(mapper.Map<SkillEntity>(skill));
            var res = await repository.SaveChangesAsync();
            if (res)
            {
                return true;
            }

            throw new Exception("Database Exception");
        }
        private async Task validateCharacterAsync(int id)
        {
            var characterEntity = await repository.GetCharacterAsync(id);
            if (characterEntity == null)
            {
                throw new NotFoundException($"the Character with the id: {id} doesn't exist");
            }
        }
    }
}
