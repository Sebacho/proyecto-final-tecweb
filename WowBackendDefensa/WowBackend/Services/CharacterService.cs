﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;
using WowBackend.Data.Repository;
using WowBackend.Exceptions;
using WowBackend.Models;

namespace WowBackend.Services
{
    public class CharacterService : ICharacterService
    {
        private IWorldOfWarcraftRepository repository;

        private List<string> allowedSortValues = new List<string>() { "id", "name", "itemlvl", "class", "server" };
        private readonly IMapper mapper;
        public CharacterService(IWorldOfWarcraftRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task<CharacterModel> CreateCharacterAsync(CharacterModel newCharacter)
        {
            var characterEntity = mapper.Map<CharacterEntity>(newCharacter);
            repository.CreateCharacter(characterEntity);
            var res = await repository.SaveChangesAsync();
            if(res)
            {
                return mapper.Map<CharacterModel>(characterEntity);
            }

            throw new Exception("Database Exception");
        }

        public async Task<bool> DeleteCharacterAsync(int id)
        {
            var deteledCharacter = await GetCharacterAsync(id);
            await repository.DeleteCharacterAsync(id);
            var res = await repository.SaveChangesAsync();
            if (res)
            {
                return true;
            }

            throw new Exception("Database Exception");
           

        }

        public async Task<CharacterModel> GetCharacterAsync(int id, bool showSkills = false)
        {
            var characterEntity = await repository.GetCharacterAsync(id, showSkills);
            if (characterEntity == null)
            {
                throw new NotFoundException($"the id : {id} doesn't exist");
            }
            else
            {
                return mapper.Map<CharacterModel>(characterEntity);
            }
        }
        public async Task<IEnumerable<SkillModel>> GetSkillsId(int id)
        {
            var skills = await repository.GetSkillsAsync(id);
            return mapper.Map<IEnumerable<SkillModel>>(skills);
        }
        public async Task<IEnumerable<CharacterModel>> GetCharactersAsync(string searchBy = "Id", string searchFor = " ", string orderBy = "id", bool showSkills = false, int famous = 0)
        {
            if (!allowedSortValues.Contains(orderBy.ToLower()))
            {
                throw new BadOperationRequest($"bad sort value: {orderBy}, allowed values are: {String.Join(",", allowedSortValues)}");
            }
            //if (famoues != 0)
            //{
            //    var characterEntities = await repository.GetFamousCharactersAs(famous);
            //}
            var characterEntities = await repository.GetCharactersAsync(famous, searchBy, searchFor, orderBy, showSkills);
            return mapper.Map<IEnumerable<CharacterModel>>(characterEntities);
        }

        public async Task<bool> UpdateCharacterAsync(CharacterModel character, int id)
        {
            await GetCharacterAsync(id);
            character.Id = id;
            repository.UpdateCharacter(mapper.Map<CharacterEntity>(character));
            var res = await repository.SaveChangesAsync(); 
            if (res)
            {
                return true;
            }

            throw new Exception("Database Exception");
        }
        public async Task<bool> AddVoteCharacterAsync(int id)
        {
            var character = await GetCharacterAsync(id);
            character.Fame = character.Fame + 1;
            repository.UpdateCharacter(mapper.Map<CharacterEntity>(character));
            var res = await repository.SaveChangesAsync();
            if (res)
            {
                return true;
            }
            throw new Exception("Database Exception");

        }
    }
}
