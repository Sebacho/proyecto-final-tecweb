﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Exceptions;
using WowBackend.Models;
using WowBackend.Services;

namespace WowBackend.Controllers
{
    [Route("api/Characters/{characterId:int}/[Controller]")]
    public class SkillsController : Controller
    {
        private ICharacterService serviceCharacters;
        private ISkillService service;
        public SkillsController(ISkillService service, ICharacterService serviceCharacters)
        {
            this.service = service;
            this.serviceCharacters = serviceCharacters;
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<SkillModel>> GetSkillAsync(int characterId, int id)
        {
            try
            { 
                return Ok(await service.GetSkillAsync(characterId, id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);

            }
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SkillModel>>> GetSkillsAsync(int CharacterId)
        {
            try
            {
                return Ok(await service.GetSkillsAsync(CharacterId));
            }
            catch (BadOperationRequest ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }
        [HttpPost]
        public async Task<ActionResult<SkillModel>> createSkillAsync(int characterId, [FromBody] SkillModel Skill)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var newSkill = await service.CreateSkillAsync(characterId, Skill);
                return Created($"/api/Characters/{characterId}/Skills/{newSkill.Id}", newSkill);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> deleteSkillAsync(int characterId, int id)
        {
            try
            {
                return Ok(await service.DeleteSkillAsync(characterId, id));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPut("{id:int}")]
        public async Task<ActionResult<bool>> updateSkillAsync(int characterId, [FromBody] SkillModel skill, int id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var state in ModelState)
                    {
                        if (state.Key == nameof(skill.Name) && state.Value.Errors.Count > 0)
                        {
                            return BadRequest(state.Value.Errors);
                        }
                    }
                }
                return Ok(await service.UpdateSkillAsync(characterId, skill, id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpDelete("{id:int}/Migrate/{nameCharacter:regex(^[[a-zA-Z]])}")]
        public async Task<ActionResult<bool>> MigrateSkillAsync(int characterId, int id, string nameCharacter)
        {
            try
            {
                var searchBy = "name";
                var searchfor = nameCharacter;
                var character = await serviceCharacters.GetCharactersAsync(searchBy, searchfor);
                var idcharacter = character.FirstOrDefault(c => c.Name == nameCharacter).Id;
                var skill = await service.GetSkillAsync(characterId, id);
                skill.Id = 0;
                var newSkill = await service.CreateSkillAsync(idcharacter, skill);
                return Ok(await service.DeleteSkillAsync(characterId, id));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
