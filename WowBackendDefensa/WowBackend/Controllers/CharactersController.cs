﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Exceptions;
using WowBackend.Models;
using WowBackend.Services;

namespace WowBackend.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[Controller]")]
    public class CharactersController : Controller
    {
        private IEnumerable<SkillModel> skillsId;
        private ICharacterService service;
        private ISkillService service2;
        public CharactersController(ICharacterService service,ISkillService service2)
        {
            this.service = service;
            this.service2 = service2;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterModel>>> GetCharacters(string searchBy = "Id", string searchfor = " ", string orderBy = "Id", bool showSkills = false, int famous = 0)
        {
            try
            {
                return Ok(await service.GetCharactersAsync(searchBy, searchfor, orderBy, showSkills, famous));
            }
            catch (BadOperationRequest ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }

        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CharacterModel>> GetCharacter(int id)
        {
            try
            {
                return Ok(await service.GetCharacterAsync(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);

            }
        }
 
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteCharacterAsync(int id)
        {
            try
            {
               
                skillsId= await service.GetSkillsId(id);
                foreach (var item in skillsId)
                {
                    await service2.DeleteSkillAsync(id,item.Id);
                }
                return Ok(await service.DeleteCharacterAsync(id));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        
        [HttpPost]
        public async Task<ActionResult<CharacterModel>> CreateCharacter([FromBody] CharacterModel Character)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var newCharacter = await service.CreateCharacterAsync(Character);
                return Created($"/api/Characters/{newCharacter.Id}", newCharacter);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
 
        [HttpPut("{id:int}")]
        public async Task<ActionResult<bool>> UpdateCharacter(int id, [FromBody] CharacterModel Character)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var state in ModelState)
                    {
                        if (state.Key == nameof(Character.Name) && state.Value.Errors.Count > 0)
                        {
                            return BadRequest(state.Value.Errors);
                        }
                    }
                }
                return Ok(await service.UpdateCharacterAsync(Character, id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut("Fame/{id:int}")]
        public async Task<ActionResult<bool>> VotesCharacter(int id)
        {
            try
            {
                return Ok(await service.AddVoteCharacterAsync(id));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
