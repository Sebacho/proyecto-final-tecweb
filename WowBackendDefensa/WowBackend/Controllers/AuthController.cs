﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WowBackend.Models.Auth;
using WowBackend.Services;

namespace WowBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IUserService userService;

        public AuthController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost("User")]

        public async Task<IActionResult> RegisterAsync([FromBody] RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.RegisterUserAsync(model);

                if (result.IsSuccess)
                    return Ok(result);

                return BadRequest(result);
            }

            return BadRequest("Some properties are not valid");
        }

        [HttpPost("Role")]
        public async Task<IActionResult> CreateRoleAsync([FromBody] CreateRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await userService.CreateRoleAsync(model);

                if(result.IsSuccess)
                {
                    return Ok(result);
                }
                return BadRequest(result);
            }
            return BadRequest("Some properties are not valid");
        }
        [HttpPost("UserRole")]
        public async Task<IActionResult> CreateUserRoleAsync([FromBody] CreateUserRoleViewModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await userService.CreateUserRoleAsync(model);

                if(result.IsSuccess)
                {
                    return Ok(result);
                }  
                return BadRequest(result);
            }
            return BadRequest("some properties are not valid");
        }
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody]LoginModel model)
        {
            if(ModelState.IsValid)
            {
                var result = await userService.LoginUserAsync(model);
                if (result.IsSuccess)
                {
                    return Ok(result);
                }

                return BadRequest(result);
            }

            return BadRequest("some properties are not valid");
        }
    }
}