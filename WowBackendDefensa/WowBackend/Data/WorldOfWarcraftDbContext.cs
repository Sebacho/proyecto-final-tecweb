﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;

namespace WowBackend.Data
{
    public class WorldOfWarcraftDbContext : IdentityDbContext
    {
        public DbSet<CharacterEntity> characters { get; set; }
        public DbSet<SkillEntity> skills { get; set; }
        public WorldOfWarcraftDbContext(DbContextOptions<WorldOfWarcraftDbContext> options) 
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CharacterEntity>().ToTable("Characters");
            modelBuilder.Entity<CharacterEntity>().HasMany(c => c.Skills).WithOne(s => s.Character);
            modelBuilder.Entity<CharacterEntity>().Property(c => c.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<SkillEntity>().ToTable("Skills");
            modelBuilder.Entity<SkillEntity>().HasOne(s => s.Character).WithMany(s => s.Skills);
            modelBuilder.Entity<SkillEntity>().Property(s => s.Id).ValueGeneratedOnAdd();
        }
    }
}
