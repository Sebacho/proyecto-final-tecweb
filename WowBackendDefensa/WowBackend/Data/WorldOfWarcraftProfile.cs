﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;
using WowBackend.Models;
using AutoMapper;

namespace WowBackend.Data
{
    public class WorldOfWarcraftProfile : Profile
    {
        public WorldOfWarcraftProfile()
        {
            this.CreateMap<CharacterEntity, CharacterModel>()
                .ReverseMap();

            this.CreateMap<SkillEntity, SkillModel>()
                .ReverseMap();

            //this.CreateMap<GameEntity, GameModel>()
            //.ReverseMap();
        }
    }
}
