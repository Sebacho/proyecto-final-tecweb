﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WowBackend.Data.Entities
{
    public class CharacterEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? ItemLvl { get; set; }
        public string Class { get; set; }
        public string Server { get; set; }
        public int Fame { get; set; }
        public string imgUrl { get; set; }
        public virtual ICollection<SkillEntity> Skills { get; set; }
    }
}
