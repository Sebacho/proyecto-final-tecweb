﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WowBackend.Data.Entities
{
    public class SkillEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string imgSkillUrl { get; set; }
        [ForeignKey("CharacterId")]
        public virtual CharacterEntity Character { get; set; }
    }
}
