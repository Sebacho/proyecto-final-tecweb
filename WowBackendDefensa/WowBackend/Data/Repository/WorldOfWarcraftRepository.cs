﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;

namespace WowBackend.Data.Repository
{
    public class WorldOfWarcraftRepository : IWorldOfWarcraftRepository
    {
        private WorldOfWarcraftDbContext dbContext;
        public WorldOfWarcraftRepository(WorldOfWarcraftDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        
        public void CreateCharacter(CharacterEntity newCharacter)
        {
            dbContext.characters.Add(newCharacter);
        }

        public void CreateSkill(SkillEntity newSkill)
        {
            dbContext.Entry(newSkill.Character).State = EntityState.Unchanged;
            dbContext.skills.Add(newSkill);
        }

        public async Task<bool> DeleteCharacterAsync(int id)
        {
            var character = await dbContext.characters.FirstOrDefaultAsync(c => c.Id == id);
            dbContext.characters.Remove(character);
            return true;
        }

        public async Task<bool> DeleteSkillAsync(int id)
        {
            var skill = await GetSkillAsync(id);
            dbContext.skills.Remove(skill);
            return true;
        }

        public async Task<CharacterEntity> GetCharacterAsync(int id, bool showSkills = false)
        {
            IQueryable<CharacterEntity> query = dbContext.characters;

            query = query.AsNoTracking();

            return await query.FirstOrDefaultAsync(d => d.Id == id);
        }

        public async Task<IEnumerable<CharacterEntity>> GetCharactersAsync(int famous = 0, string searchBy = "Id", string searchFor = " ", string orderby = "id", bool showSkills = false)
        {
            IQueryable<CharacterEntity> query = dbContext.characters;

            if (famous != 0)
            {
                query = query.OrderByDescending(c => c.Fame).Take(famous);
            }

            switch (searchBy)
            {
                case "name":
                    query = query.Where(c => c.Name == searchFor);
                    break;
                case "class":
                    query = query.Where(c => c.Class == searchFor);
                    break;
                case "server":
                    query = query.Where(c => c.Server == searchFor);
                    break;
                default:
                    break;
            }
            switch (orderby)
            {
                case "id":
                    query = query.OrderBy(c => c.Id);
                    break;
                case "name":
                    query = query.OrderBy(c => c.Name);
                    break;
                case "itemlvl":
                    query = query.OrderBy(c => c.ItemLvl);
                    break;
                case "class":
                    query = query.OrderBy(c => c.Class);
                    break;
                case "server":
                    query = query.OrderBy(c => c.Server);
                    break;
                default:
                    break;
            }
            if (showSkills) 
            {
                query = query.Include(c => c.Skills);
            }

            query = query.AsNoTracking();

            return await query.ToArrayAsync();
        }
        
        //public async Task<IEnumerable<CharacterEntity>> GetFamousCharactersAsyn(int famous)
        //{
            
        //    return await dbContext.characters.OrderByDescending(c => c.Fame).Take(famous);
        //}

        public async Task<SkillEntity> GetSkillAsync(int id)
        {
            IQueryable<SkillEntity> query = dbContext.skills;
            query = query.Include(s => s.Character);
            query = query.AsNoTracking();

            return await query.SingleOrDefaultAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<SkillEntity>> GetSkillsAsync(int characterId)
        {

            //return skills.Where(skill => skill.CharacterId == characterId);
            IQueryable<SkillEntity> query = dbContext.skills;
            query = query.Include(s => s.Character);

            query = query.AsNoTracking();
            query = query.Where(s => s.Character.Id == characterId);
            return await query.ToArrayAsync();
        }

        public async Task<bool> SaveChangesAsync()
        {
            var res = await dbContext.SaveChangesAsync();
            return res > 0; 
        }

        public bool UpdateCharacter(CharacterEntity character)
        {
            var res = dbContext.characters.FirstOrDefault(c => c.Id == character.Id);
            res.Name = character.Name ?? res.Name;
            res.ItemLvl = character.ItemLvl ?? res.ItemLvl;
            res.Class = character.Class ?? res.Class;
            res.Server = character.Server ?? res.Server;
            res.imgUrl = character.imgUrl ?? res.imgUrl;
            if(character.Fame != 0)
            {
                res.Fame = character.Fame;
            }
            dbContext.characters.Update(res);
            return true;
        }

        public bool UpdateSkill(SkillEntity skill)
        {

            var res = dbContext.skills.FirstOrDefault(s=> s.Id==skill.Id);
            res.Name = skill.Name ?? res.Name;
            res.Description = skill.Description ?? res.Description;
            dbContext.Entry(skill.Character).State = EntityState.Unchanged;
            dbContext.skills.Update(res);
            return true;
        } 
    }
}
