﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WowBackend.Data.Entities;

namespace WowBackend.Data.Repository
{
    public interface IWorldOfWarcraftRepository
    {
        Task<CharacterEntity> GetCharacterAsync(int id, bool showSkills = false);
        Task<IEnumerable<CharacterEntity>> GetCharactersAsync(int famous = 0, string searchBy = "id", string searchFor = " ", string orderBy = "id", bool showSkills = false);
       // Task<IEnumerable<CharacterEntity>> GetFamousCharactersAsync(int famous);
        void CreateCharacter(CharacterEntity newCharacter);
        bool UpdateCharacter(CharacterEntity character);
        Task<bool> DeleteCharacterAsync(int id);

        //Skills
        Task<SkillEntity> GetSkillAsync(int id);
        Task<IEnumerable<SkillEntity>> GetSkillsAsync(int characterId);
        void CreateSkill(SkillEntity newSkill);
        bool UpdateSkill(SkillEntity skill);
        Task<bool> DeleteSkillAsync(int id);

        //Save changes
        Task<bool> SaveChangesAsync();
    }
}
