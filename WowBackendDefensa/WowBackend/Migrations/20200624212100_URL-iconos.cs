﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WowBackend.Migrations
{
    public partial class URLiconos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "URL",
                table: "Skills",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "URL",
                table: "Characters",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "URL",
                table: "Skills");

            migrationBuilder.DropColumn(
                name: "URL",
                table: "Characters");
        }
    }
}
