﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WowBackend.Migrations
{
    public partial class itemLevelMayus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Itemlvl",
                table: "Characters",
                newName: "ItemLvl");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ItemLvl",
                table: "Characters",
                newName: "Itemlvl");
        }
    }
}
