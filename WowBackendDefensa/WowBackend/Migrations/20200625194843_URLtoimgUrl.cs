﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WowBackend.Migrations
{
    public partial class URLtoimgUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "URL",
                table: "Skills",
                newName: "imgSkillUrl");

            migrationBuilder.RenameColumn(
                name: "URL",
                table: "Characters",
                newName: "imgUrl");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "imgSkillUrl",
                table: "Skills",
                newName: "URL");

            migrationBuilder.RenameColumn(
                name: "imgUrl",
                table: "Characters",
                newName: "URL");
        }
    }
}
