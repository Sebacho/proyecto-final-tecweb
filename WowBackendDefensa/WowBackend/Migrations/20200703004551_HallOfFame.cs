﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WowBackend.Migrations
{
    public partial class HallOfFame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Fame",
                table: "Characters",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Fame",
                table: "Characters");
        }
    }
}
